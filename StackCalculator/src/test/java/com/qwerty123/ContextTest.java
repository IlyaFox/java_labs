package com.qwerty123;

import com.qwerty123.exception.DefineException;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import java.util.EmptyStackException;

public class ContextTest {
    private static final double DELTA = 1E-5;
    private Context context;

    @Before
    public void initContext() {
        context = new Context();
        context.push(-7.0);
        context.push(3.0);
    }

    @Test
    public void sizeStack() {
        Assert.assertEquals(2, context.sizeStack());
    }

    @Test(expected = EmptyStackException.class)
    public void pop() {
        double a = context.pop();
        double b = context.pop();
        Assert.assertEquals(a, 3.0, DELTA);
        Assert.assertEquals(b, -7.0, DELTA);
        context.pop();
    }

    @Test(expected = EmptyStackException.class)
    public void peek() {
        double a = context.peek();
        double b = context.peek();
        Assert.assertEquals(a, 3.0, DELTA);
        Assert.assertEquals(b, 3.0, DELTA);
        context.pop();
        context.pop();

        context.peek();
    }

    @Test
    public void setDefine() {
        context.setDefine("pi", 3.14);
        context.setDefine("exp", 2.7);
        context.push("pi");
        context.push("exp");

        Assert.assertEquals(2.7, context.pop(), DELTA);
        Assert.assertEquals(3.14, context.pop(), DELTA);
    }

    @Test(expected = DefineException.class)
    public void pushStringThrowException() {
        context.push("pi");
    }
}
