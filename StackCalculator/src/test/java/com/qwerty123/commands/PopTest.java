package com.qwerty123.commands;

import com.qwerty123.Arguments;
import com.qwerty123.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

import java.util.EmptyStackException;

public class PopTest {
    private Context context;
    private Pop cmd;
    private static final String[] ARGS = "foo 3.14 10000000 -8".split(" ");
    private static final double DELTA = 1E-5;

    @Before
    public void initContext() {
        context = new Context();
        cmd = new Pop();
    }

    @Test
    public void popTest() {
        context.push(1.0);
        context.push(2.0);
        cmd.execute(new Arguments(new String[]{""}), context);
        Assert.assertEquals(1.0, context.peek(), DELTA);
    }

    @Test
    public void badArgsTest() {
        context.push(5.5);
        cmd.execute(new Arguments(ARGS), context);
    }

    @Test(expected = EmptyStackException.class)
    public void emptyStackTest() {
        cmd.execute(new Arguments(ARGS), context);
    }
}
