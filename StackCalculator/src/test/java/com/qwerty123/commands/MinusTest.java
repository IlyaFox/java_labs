package com.qwerty123.commands;

import com.qwerty123.Arguments;
import com.qwerty123.Context;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class MinusTest {
    private Context context;
    private Minus cmd;
    private static final String[] ARGS = "foo 3.14 10000000 -8".split(" ");
    private static final double DELTA = 1E-5;

    @Before
    public void initContext() {
        context = new Context();
        cmd = new Minus();
    }

    @Test
    public void minusTest() {
        context.push(-90.123456789);
        context.push(-0.0);
        cmd.execute(new Arguments(new String[]{" "}), context);
        Assert.assertEquals(-0.0 - -90.123456789, context.pop(), DELTA);
    }

    @Test
    public void badArgsTest() {
        context.push(9.0);
        context.push(1.0E+10);
        cmd.execute(new Arguments(ARGS), context);
        Assert.assertEquals(1.0E+10 - 9.0, context.pop(), DELTA);
    }
}
