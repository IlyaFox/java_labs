package com.qwerty123.commands;

import com.qwerty123.Arguments;
import com.qwerty123.Context;
import com.qwerty123.exception.ArgsException;
import com.qwerty123.exception.DefineException;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;


public class DefineTest {
    private Context context;
    private Define cmd;
    private static final String[] ARGS = "foo 3.14 10000000 -8".split(" ");
    private static final double DELTA = 1E-5;

    @Before
    public void initContext() {
        context = new Context();
        cmd = new Define();
    }

    @Test
    public void popTest() {
        cmd.execute(new Arguments(new String[]{"pi","3.14"}), context);
        cmd.execute(new Arguments(new String[]{"pi","0.5"}), context);
        context.push("pi");
        Assert.assertEquals(0.5, context.peek(), DELTA);
    }

    @Test(expected = NumberFormatException.class)
    public void badArgsTest() {
        cmd.execute(new Arguments(new String[]{"pi","3.14qqqq"}), context);
    }

    @Test(expected = ArgsException.class)
    public void emptyStackTest() {
        cmd.execute(new Arguments(ARGS), context);
    }
}
