package com.qwerty123.commands;

import com.qwerty123.Arguments;
import com.qwerty123.Context;
import com.qwerty123.exception.ArgsException;
import com.qwerty123.exception.DefineException;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class PushTest {
    private Context context;
    private Push cmd;
    private static final String[] ARGS = "foo 3.14 10000000 -8".split(" ");
    private static final double DELTA = 1E-5;

    @Before
    public void initContext() {
        context = new Context();
        cmd = new Push();
    }

    @Test
    public void pushTest() {
        cmd.execute(new Arguments(new String[]{" 123.123456789 "}), context);
        Assert.assertEquals(123.123456789, context.pop(), DELTA);
    }


    @Test(expected = DefineException.class)
    public void badNumTest() {
        cmd.execute(new Arguments(new String[]{"123.fooooo345"}), context);
    }

    @Test(expected = ArgsException.class)
    public void badArgsTest() {
        cmd.execute(new Arguments(ARGS), context);
    }
}
