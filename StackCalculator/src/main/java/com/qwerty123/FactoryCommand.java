package com.qwerty123;

import com.qwerty123.exception.UnknownClassName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.qwerty123.exception.FactoryException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class FactoryCommand {
    private static final Logger logger = LogManager.getLogger();

    private Properties properties = new Properties();

    public FactoryCommand() throws FactoryException {
        logger.info("Open/Loading commands.properties");
        try (InputStream in = getClass().getClassLoader().getResourceAsStream("commands.properties")) {
            properties.load(in);
        } catch (IOException | NullPointerException err) {
            throw new FactoryException("File: " + "\"commands.properties\"" + " not found", err);
        }
    }

    public ICommand getCommand(String cmdName) throws FactoryException, UnknownClassName {
        try {
            String className = properties.getProperty(cmdName.toLowerCase());
            if (className == null) {
                throw new UnknownClassName("Unknown class name: " + cmdName, cmdName);
            }
            return (ICommand) Class.forName(className).newInstance();

        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException err) {
            throw new FactoryException("Class instantiation error ", err);
        }
    }
}
