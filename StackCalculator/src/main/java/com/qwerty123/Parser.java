package com.qwerty123;

import com.qwerty123.exception.FactoryException;
import com.qwerty123.exception.UnknownClassName;

public class Parser {
    private FactoryCommand factory;
    private Pair<ICommand, Arguments> pair;

    public Parser() throws FactoryException {
        factory = new FactoryCommand();
    }

    public Pair<ICommand, Arguments> parse(String line) throws FactoryException, UnknownClassName {
        String[] words = line.split(" ");
        String[] args = new String[words.length - 1];
        System.arraycopy(words, 1, args, 0, words.length - 1);
        pair = new Pair<>(factory.getCommand(words[0]), new Arguments(args));
        return pair;
    }

    public Pair<ICommand, Arguments> getPair() {
        return pair;
    }
}
