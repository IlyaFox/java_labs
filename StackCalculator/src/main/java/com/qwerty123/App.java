package com.qwerty123;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class App {
    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        logger.info("Starting program");
        Calculator calc = new Calculator();
        Scanner in;
        if (args.length != 0) {
            try {
                in = new Scanner(new FileInputStream(args[0]));
                logger.debug("Open file: " + args[0]);
            } catch (IOException err) {
                logger.fatal("Opening the: " + args[0] + " failed. " + err.getMessage());
                return;
            }
        } else {
            logger.info("Input of stdin (console)");
            in = new Scanner(System.in);
        }
        try {
            calc.calculate(in);
        } catch (Exception e) {
            logger.fatal("Error message: " + e.getMessage());
            e.printStackTrace();
        }
        in.close();
        logger.info("Program finished");
    }

}
