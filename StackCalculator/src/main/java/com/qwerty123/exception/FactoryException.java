package com.qwerty123.exception;

public class FactoryException extends Exception {
    public FactoryException(String message) {
        super(message);
    }

    public FactoryException(String message, Exception cause) {
        super(message, cause);
    }
}
