package com.qwerty123.exception;

public class ArgsException extends IllegalArgumentException {
    public ArgsException(String message) {
        super(message);
    }

    public ArgsException(String message, Exception cause) {
        super(message, cause);
    }
}
