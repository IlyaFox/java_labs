package com.qwerty123.exception;

public class UnknownClassName extends Exception {
    private String badName;

    public UnknownClassName(String message, String badName) {
        super(message);
        this.badName = badName;
    }

    public UnknownClassName(String message, Exception cause, String badName) {
        super(message, cause);
        this.badName = badName;
    }

    public String getBadName(){
        return badName;
    }
}
