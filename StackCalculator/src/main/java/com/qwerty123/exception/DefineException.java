package com.qwerty123.exception;

public class DefineException extends RuntimeException {
    public DefineException(String message) {
        super(message);
    }

    public DefineException(String message, Exception cause) {
        super(message, cause);
    }
}
