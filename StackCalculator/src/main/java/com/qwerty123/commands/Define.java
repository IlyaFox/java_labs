package com.qwerty123.commands;

import com.qwerty123.Arguments;
import com.qwerty123.Context;
import com.qwerty123.ServiceOperation;
import com.qwerty123.exception.ArgsException;

public class Define extends ServiceOperation {
    @Override
    public void execute(Arguments argv, Context context) {
        if (argv.size() != 2) {
            throw new ArgsException("Bad qty args for Define. Expected: 2, have: " + argv.size());
        }
        String name = argv.getNextArg();
        context.setDefine(name, Double.parseDouble(argv.getNextArg()));
    }
}
