package com.qwerty123.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.qwerty123.Arguments;
import com.qwerty123.Context;
import com.qwerty123.UnaryOperation;

public class Sqrt extends UnaryOperation {
    private static final Logger logger = LogManager.getLogger();

    @Override
    public void execute(Arguments argv, Context context) {
        if (argv.size() != 0) {
            logger.warn(getClass().getName() + " have " + argv.size() + " args, but expected: 0");
        }
        double tmp = context.pop();
        if (tmp < 0) {
            throw new ArithmeticException("Sqrt of a negative number: " + argv.size());
        }
        context.push(Math.sqrt(tmp));
    }
}
