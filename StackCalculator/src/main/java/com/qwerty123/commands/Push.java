package com.qwerty123.commands;

import com.qwerty123.Arguments;
import com.qwerty123.Context;
import com.qwerty123.ServiceOperation;
import com.qwerty123.exception.ArgsException;

public class Push extends ServiceOperation {
    private boolean isDouble(String string) {
        try {
            Double.parseDouble(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void execute(Arguments argv, Context context) {
        if (argv.size() != 1) {
            throw new ArgsException("Bad qty args for Push. Expected: 1, have: " + argv.size());
        }
        String tmp = argv.getNextArg();
        if (isDouble(tmp)) {
            context.push(Double.parseDouble(tmp));
        } else {
            context.push(tmp);
        }
    }
}
