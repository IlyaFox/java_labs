package com.qwerty123;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.qwerty123.exception.DefineException;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Context {
    public static final Logger logger = LogManager.getLogger();

    private Stack<Double> stack = new Stack<>();
    private Map<String, Double> define = new HashMap<>();

    public int sizeStack() {
        return stack.size();
    }

    public double peek() {
        if (stack.empty()) {
            throw new EmptyStackException();
        }
        return stack.peek();
    }

    public double pop() {
        if (stack.empty()) {
            throw new EmptyStackException();
        }
        return stack.pop();
    }

    public void push(Double number) {
        stack.push(number);
    }

    public void push(String nameConst) {
        nameConst = nameConst.toLowerCase();
        if (define.containsKey(nameConst)) {
            push(define.get(nameConst));
        } else {
            logger.error("Define '" + nameConst + "' not exist");
            throw new DefineException(nameConst);
        }
    }

    public void setDefine(String nameConst, Double value) {
        define.put(nameConst.toLowerCase(), value);
    }

}
