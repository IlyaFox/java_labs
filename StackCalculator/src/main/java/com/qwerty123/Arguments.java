package com.qwerty123;

public class Arguments {
    private String[] args;
    private int id = 0;

    public Arguments(String[] args) {
        this.args = args.clone();
    }

    public int size() {
        return args.length;
    }

    public String getNextArg() {
        if (id == args.length) {
            id = 0;
        }
        return args[id++];
    }
}
