package com.qwerty123;

public interface ICommand {
    void execute(Arguments argv, Context context);
    boolean checkContext(Context context);
}
