package com.qwerty123;

import com.qwerty123.exception.FactoryException;
import com.qwerty123.exception.UnknownClassName;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Calculator {
    private static final Logger logger = LogManager.getLogger();
    private Context context = new Context();

    public void calculate(Scanner in) throws FactoryException {
        Parser parser = new Parser();

        while (in.hasNextLine()) {
            String line = in.nextLine();
            if (line.isEmpty()) {
                logger.warn("Found empty string.");
                continue;
            }
            if (line.charAt(0) == '#') {
                continue;
            }
            try {
                Pair<ICommand, Arguments> pair = parser.parse(line);
                if (pair.fst.checkContext(context)) {
                    logger.debug("Running " + pair.fst);
                    pair.fst.execute(pair.snd, context);
                } else {
                    logger.error("Bad context for:  " + pair.fst);
                    continue;
                }
            } catch (UnknownClassName err) {
                System.out.println("Invalid command, please try again...");
            }
        }
    }
}
