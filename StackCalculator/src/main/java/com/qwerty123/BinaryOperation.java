package com.qwerty123;

public abstract class BinaryOperation implements ICommand {

    @Override
    public boolean checkContext(Context context) {
        return context.sizeStack() >= 2;
    }
}
