package com.qwerty123;

import com.qwerty123.model.Context;
import com.qwerty123.model.Obstacle;
import com.qwerty123.model.ResourceManager;
import com.qwerty123.view.Canvas;
import com.qwerty123.view.MainWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

public class Game implements ActionListener {
    private ResourceManager resourceManager;
    private KeyController keyController;
    private MainWindow window;
    private Canvas canvas;

    private Timer timer = new Timer(20, this);

    public Game() {
        resourceManager = new ResourceManager();
        keyController = new KeyController(resourceManager);
        canvas = new Canvas(resourceManager, keyController);
        window = new MainWindow(canvas);
        timer.start();
    }


    private void moveVisibleObstacle() {
        Iterator<Obstacle> it = resourceManager.getObstaclesList().iterator();
        while (it.hasNext()) {
            Obstacle obstacle = it.next();
            if (obstacle.getX() + obstacle.getWidth() < 0) {
                it.remove();
                continue;
            }
            obstacle.move();
        }
    }

    private boolean isHeroHit() {
        for (Obstacle it : resourceManager.getObstaclesList()) {
            if (resourceManager.getHero().getHitBox().intersects(it.getHitBox())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (resourceManager.getContext() == Context.IN_GAME) {
            resourceManager.increaseDistance();
            resourceManager.getBackgroundsLayers()[0].move();
            resourceManager.getBackgroundsLayers()[1].move();
            if (resourceManager.getHero().isJumping()) {
                resourceManager.getHero().jump();
            } else {
                resourceManager.getHero().move();
            }
            moveVisibleObstacle();
            if (isHeroHit() && (resourceManager.getHittableDist() < resourceManager.getDistance())) {
                resourceManager.setCurLives(resourceManager.getCurLives() - 1);
                if (resourceManager.getCurLives() == 0) {
                    resourceManager.gameOver();
                } else {
                    resourceManager.setHittableDist(resourceManager.getDistance() + 500);
                }
            }
        }
        canvas.update();
    }
}
