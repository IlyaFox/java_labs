package com.qwerty123.model;

import java.awt.*;

public abstract class Sprite {
    private Image currentImage;

    private int width = 0;
    private int height = 0;

    protected int x = 0;
    protected int y = 0;

    protected int speedX = 0;
    protected int speedY = 0;

    protected int accelerationX = 0;
    protected int accelerationY = 0;

    public Sprite(Image currentImage) {
        this.currentImage = currentImage;
    }

    public abstract void move();

    public void initParameters(int x, int y, int width, int height, int speedX, int speedY, int accelerationX, int accelerationY) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        this.speedX = speedX;
        this.speedY = speedY;
        this.accelerationX = accelerationX;
        this.accelerationY = accelerationY;
    }

    public void setCurrentImage(Image image){
        this.currentImage = image;
    }

    public Image getCurrentImage() {
        return currentImage;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getSpeedX() {
        return speedX;
    }

    public int getSpeedY() {
        return speedY;
    }

    public int getAccelerationX() {
        return accelerationX;
    }

    public int getAccelerationY() {
        return accelerationY;
    }
}
