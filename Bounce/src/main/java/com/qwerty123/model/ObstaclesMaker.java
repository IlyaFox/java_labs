package com.qwerty123.model;

import java.util.Random;

public class ObstaclesMaker implements Runnable {
    public static final int OBSTACLES_TIME_BOUND = 3200;
    public static final int MIN_CREATE_TIME = 700;

    private Thread maker = new Thread(this);
    private Random rand = new Random();
    private ResourceManager resourceManager;

    public ObstaclesMaker(ResourceManager resourceManager) {
        this.resourceManager = resourceManager;
        maker.start();
    }

    public void stop(){
        maker.interrupt();
    }

    @Override
    public void run() {
        while (resourceManager.getContext() == Context.IN_GAME) {
            try {
                Thread.sleep(rand.nextInt(OBSTACLES_TIME_BOUND - MIN_CREATE_TIME) + MIN_CREATE_TIME);
                resourceManager.addObstacle(rand);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
