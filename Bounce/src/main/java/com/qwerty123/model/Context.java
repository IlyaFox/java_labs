package com.qwerty123.model;

public enum Context {
    IN_MENU,
    IN_GAME,
    GAME_OVER
}
