package com.qwerty123.model;

import com.qwerty123.view.MainWindow;

import java.awt.*;
import java.util.ArrayList;


public class Background extends Sprite {
    public static final int BACKGROUND_Y = 0;
    public static final double SCALE = 0.5;


    ArrayList<Image> maps;
    private int cadreCounter = 0;
    private int curMap = 0;
    private int change_map_dist = 0;

    public Background(ArrayList<Image> maps, int x, int speed_x) {
        super(maps.get(0));
        this.maps = maps;
        int width = (int) (maps.get(0).getWidth(null) * SCALE);
        int height = (int) (maps.get(0).getHeight(null) * SCALE);
        initParameters(x, BACKGROUND_Y, width, height, speed_x, 0, 0, 0);
        change_map_dist = speed_x / 3 + 1;

    }

    public void changeMap() {
        curMap = (curMap + 1) % maps.size();
        setCurrentImage(maps.get(curMap));
        cadreCounter = 0;
    }

    @Override
    public void move() {
        if (x + MainWindow.WINDOW_WIDTH <= speedX) {
            x = MainWindow.WINDOW_WIDTH;
            cadreCounter++;
            if (cadreCounter == change_map_dist) {
                changeMap();
            }
        }
        x -= speedX;
    }
}
