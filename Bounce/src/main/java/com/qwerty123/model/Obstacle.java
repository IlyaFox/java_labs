package com.qwerty123.model;

import com.qwerty123.view.MainWindow;

import java.awt.*;

public class Obstacle extends Sprite implements Interacting {
    public static final int OBSTACLE_X = MainWindow.WINDOW_WIDTH;
    public static final int OBSTACLE_Y_UP = Hero.HERO_Y + 224;

    public Obstacle(Image obstacle, double scale, int speed_x) {
        super(obstacle);
        int width = (int) (obstacle.getWidth(null) * scale);
        int height = (int) (obstacle.getHeight(null) * scale);
        initParameters(OBSTACLE_X, OBSTACLE_Y_UP - height, width, height, speed_x, 0, 0, 0);
    }


    @Override
    public Rectangle getHitBox() {
        return new Rectangle(x + 30, y + 15, (int) (getWidth() * 0.5), (int) (getHeight() * 0.70));
    }

    @Override
    public void move() {
        x = x - speedX;
    }

}
