package com.qwerty123.model;

import com.qwerty123.view.MainWindow;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;

public class ResourceManager {
    private SpritesPaths spritesPaths = new SpritesPaths();
    private ObstaclesMaker obstaclesMaker;
    private Context context;
    private ArrayList<Image> maps;

    private StartMenu startMenu;
    private Hero hero;
    private ArrayList<Obstacle> obstaclesList = new ArrayList<>();
    private Background[] backgroundsLayers = new Background[2];
    private int speed;
    private int curLives;
    private int sessionLives;
    private int distance;
    private int recordDistance;
    private int hittableDist;


    public ResourceManager() {
        ArrayList<Image> menuImages = getImagesFromProperties(spritesPaths.getMenu(), "menu_", "", spritesPaths.getMenu().size());
        maps = getImagesFromProperties(spritesPaths.getBackground(), "map_", "", spritesPaths.getBackground().size());
        startMenu = new StartMenu(menuImages);
        distance = 0;
        recordDistance = 0;
        context = Context.IN_MENU;
    }


    public void start(int speed) {
        setCurLives(sessionLives);
        setSpeed(speed);
        setContext(Context.IN_GAME);
        obstaclesMaker = new ObstaclesMaker(this);
        backgroundsLayers[0] = new Background(maps, 0, speed);
        backgroundsLayers[1] = new Background(maps, MainWindow.WINDOW_WIDTH, speed);
        setHittableDist(0);
    }

    public void gameOver() {
        setContext(Context.GAME_OVER);
        updateRecord();
        distance = 0;
        obstaclesList.clear();
        obstaclesMaker.stop();
    }

    public void showMenu() {
        context = Context.IN_MENU;

    }

    public void change_hero(String heroName) {
        ArrayList<Image> runImages;
        ArrayList<Image> jumpImages;

        runImages = getImagesFromProperties(spritesPaths.getHeroes(), heroName, "run_", 3);
        jumpImages = getImagesFromProperties(spritesPaths.getHeroes(), heroName, "jump_", 1);

        hero = new Hero(runImages, jumpImages);
    }

    private ArrayList<Image> getImagesFromProperties(Properties pathsProperties, String preString, String action, int qtyImages) {
        ArrayList<Image> images = new ArrayList<>(qtyImages);
        for (int i = 0; i < qtyImages; ++i) {
            String path = pathsProperties.getProperty(preString + action + i);
            Image image = new ImageIcon(path).getImage();
            images.add(image);
        }
        return images;
    }

    public void addObstacle(Random random) {
        ArrayList<Image> obstacleImages = getImagesFromProperties(spritesPaths.getObstacles(), "obstacle_", "", spritesPaths.getObstacles().size());
        obstaclesList.add(new Obstacle(obstacleImages.get(random.nextInt(obstacleImages.size())), 0.35, backgroundsLayers[0].getSpeedX()));
    }

    public void setCurLives(int curLives) {
        this.curLives = curLives;
    }

    public int getCurLives() {
        return curLives;
    }

    public int getSessionLives() {
        return sessionLives;
    }

    public void setSessionLives(int sessionLives) {
        this.sessionLives = sessionLives;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getSpeed() {
        return speed;
    }

    public int getHittableDist() {
        return hittableDist;
    }

    public void setHittableDist(int hittableDist) {
        this.hittableDist = hittableDist;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void increaseDistance() {
        this.distance += backgroundsLayers[0].speedX;
    }

    public void updateRecord() {
        recordDistance = Math.max(recordDistance, distance);
    }

    public Background[] getBackgroundsLayers() {
        return backgroundsLayers;
    }

    public Hero getHero() {
        return hero;
    }

    public StartMenu getStartMenu() {
        return startMenu;
    }

    public ArrayList<Obstacle> getObstaclesList() {
        return obstaclesList;
    }

    public Context getContext() {
        return context;
    }

    public int getDistance() {
        return distance;
    }

    public int getRecordDistance() {
        return recordDistance;
    }
}
