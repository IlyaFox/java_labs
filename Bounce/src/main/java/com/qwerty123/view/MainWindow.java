package com.qwerty123.view;

import javax.swing.*;

public class MainWindow extends JFrame {
    public static final int WINDOW_WIDTH = 1024;
    public static final int WINDOW_HEIGHT = 512;

    private java.awt.Canvas canvas;

    public MainWindow(Canvas canvas) {
        setTitle("Bounce");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT );
        setLocationRelativeTo(null);
        setResizable(false);

        ImageIcon icon = new ImageIcon("assets/icon.png");
        setIconImage(icon.getImage());

        setUndecorated(true);
        add(canvas);

        setVisible(true);
    }
}
