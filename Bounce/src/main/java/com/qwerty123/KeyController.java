package com.qwerty123;

import com.qwerty123.model.ResourceManager;

import javax.swing.*;
import java.awt.event.KeyEvent;

public class KeyController {
    ResourceManager resourceManager;

    public KeyController(ResourceManager resourceManager) {
        this.resourceManager = resourceManager;
    }

    public void keyPressed(KeyEvent event) {
        int key = event.getKeyCode();

        switch (resourceManager.getContext()) {
            case IN_GAME:
                inGameHandler(key);
                break;
            case GAME_OVER:
                gameOverHandler(key);
                break;
            case IN_MENU:
                inMenuHandler(key);
                break;
        }

        defaultHandler(key);
    }


    private void inGameHandler(int key) {
        if (key == KeyEvent.VK_SPACE) {
            resourceManager.getHero().setJumping(true);
        } else if (key == KeyEvent.VK_ESCAPE) {
            resourceManager.showMenu();
        }
    }

    private void gameOverHandler(int key) {
        if (key == KeyEvent.VK_SPACE) {
            resourceManager.start(resourceManager.getSpeed());
        } else if (key == KeyEvent.VK_ESCAPE) {
            resourceManager.showMenu();
        }
    }

    private void inMenuHandler(int key) {
        if (key == KeyEvent.VK_A || key == KeyEvent.VK_LEFT) {
            resourceManager.getStartMenu().change(false);
        } else if (key == KeyEvent.VK_D || key == KeyEvent.VK_RIGHT) {
            resourceManager.getStartMenu().change(true);
        } else if (key == KeyEvent.VK_SPACE) {
            String heroName = resourceManager.getStartMenu().getChoose();
            resourceManager.change_hero(heroName);

            int speed = (int) JOptionPane.showInputDialog(null, "Enter speed", "Speed", JOptionPane.QUESTION_MESSAGE, null, new Integer[]{10, 12, 14, 16, 18, 20}, 10);
            int lives = 4 - (int) JOptionPane.showInputDialog(null, "Choose level", "Live", JOptionPane.QUESTION_MESSAGE, null, new Integer[]{1, 2, 3}, 1);

            resourceManager.setSessionLives(lives);
            resourceManager.start(speed);
            System.out.println("Set: " + heroName);
        } else if (key == KeyEvent.VK_ESCAPE) {
            System.exit(0);
        }
    }

    private void defaultHandler(int key) {

    }

}
