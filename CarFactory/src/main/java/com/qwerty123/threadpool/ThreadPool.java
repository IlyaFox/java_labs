package com.qwerty123.threadpool;

import java.util.concurrent.*;

public class ThreadPool {
    private ExecutorService service;

    public ThreadPool(int qtyThreads) {
        this.service = Executors.newFixedThreadPool(qtyThreads);
    }

    public void shutdownThreadPool() {
        this.service.shutdownNow();
    }

    public void executeTask(Runnable task) {
        service.submit(task);
    }

    public <T> Future<T> executeTask(Callable<T> task) {
        return service.submit(task);
    }

}
