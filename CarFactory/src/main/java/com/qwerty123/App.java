package com.qwerty123;

import com.qwerty123.factory.ResourceController;

public class App {
    public static void main(String[] args) {
        ResourceController controller = new ResourceController();
        controller.start();
    }
}
