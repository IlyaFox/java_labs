package com.qwerty123.factory.model;

import com.qwerty123.threadpool.ThreadPool;

public class CarFactory {
    public static final int WORK_TIME = 1000;

    private ThreadPool threadPool;
    private Storage<Detail> accessoryStorage;
    private Storage<Detail> bodyStorage;
    private Storage<Detail> engineStorage;
    private Storage<Car> carStorage;

    public CarFactory(int qtyMechanics, Storage<Detail> accessory, Storage<Detail> body, Storage<Detail> engine, Storage<Car> car) {
        this.threadPool = new ThreadPool(qtyMechanics);

        this.accessoryStorage = accessory;
        this.bodyStorage = body;
        this.engineStorage = engine;
        this.carStorage = car;
    }

    public void createCar() {
        Runnable task = () -> {
            try {
                Detail body = bodyStorage.getItem();
                Detail engine = engineStorage.getItem();
                Detail accessory = accessoryStorage.getItem();

                Thread.sleep(WORK_TIME);

                Car car = new Car("BMW", body, engine, accessory);
                carStorage.addItem(car);
            } catch (InterruptedException ignored) {
            }
        };
        threadPool.executeTask(task);
    }

    public void stop() {
        threadPool.shutdownThreadPool();
    }
}
