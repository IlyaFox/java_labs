package com.qwerty123.factory.model;

import java.util.concurrent.TimeUnit;

public class Dealer implements Runnable {
    private CarStorageManager manager;
    private Storage<Car> carStorage;
    private int periodRequest = 1500;

    public Dealer(Storage<Car> carStorage, CarStorageManager manager) {
        this.manager = manager;
        this.carStorage = carStorage;
    }

    public Car requestCar() throws InterruptedException {
        manager.requestCar();
        return carStorage.getItem();
    }

    /**
     * @param period milliseconds
     */
    public void setPeriodRequest(int period) {
        this.periodRequest = period;
    }

    public int getPeriodRequest() {
        return periodRequest;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Car car = requestCar();
                manager.notifyAboutReceiving(car.toString() + " Dealer <" + Thread.currentThread().getName() + "> ");
                TimeUnit.MILLISECONDS.sleep(periodRequest);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
