package com.qwerty123.factory.model;

import java.util.concurrent.TimeUnit;

public class Supplier implements Runnable {
    private int periodSupply = 3000;
    private TypesDetail type;
    private Storage<Detail> customerStorage;

    public Supplier(TypesDetail type, Storage<Detail> customerStorage) {
        this.type = type;
        this.customerStorage = customerStorage;
    }

    public void deliver() throws InterruptedException {
        Detail detail = new Detail(type);
        customerStorage.addItem(detail);
    }

    /**
     * @param period milliseconds
     */
    public void setPeriodSupply(int period) {
        this.periodSupply = period;
    }

    public int getPeriodSupply() {
        return periodSupply;
    }

    @Override
    public void run() {
        while (true) {
            try {
                deliver();
                TimeUnit.MILLISECONDS.sleep(periodSupply);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
