package com.qwerty123.factory.model;

public class CarStorageManager {
    public final int MIN_CAR;

    private CarFactory mechanics;
    private Storage<Car> carStorage;
    private int soldCarCounter = 0;
    private boolean isLogging;
    private int qtyRequested = 0;

    public CarStorageManager(CarFactory mechanics, Storage<Car> carStorage, boolean isLogging) {
        MIN_CAR = (int) (carStorage.getCapacity() * 0.1);
        this.mechanics = mechanics;
        this.carStorage = carStorage;
        this.isLogging = isLogging;
    }

    synchronized public void requestCar() {
        mechanics.createCar();
        qtyRequested++;
    }

    synchronized public void fillMinimum() {
        if (qtyRequested < MIN_CAR && carStorage.getNumberItems() < MIN_CAR) {
            for (int i = 0; i < MIN_CAR - carStorage.getNumberItems(); i++) {
                mechanics.createCar();
                qtyRequested++;
            }
        }
    }

    synchronized public void notifyAboutReceiving(String message) {
        if (isLogging) {
            System.out.println(message);
        }
        soldCarCounter++;
        qtyRequested--;
    }

    public int getSoldCarCounter() {
        return soldCarCounter;
    }

    public int getQtyRequested() {
        return qtyRequested;
    }
}
