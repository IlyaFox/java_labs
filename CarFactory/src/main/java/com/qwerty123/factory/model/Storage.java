package com.qwerty123.factory.model;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.concurrent.atomic.AtomicLong;

public class Storage<T extends Identifiable> {
    private Deque<T> item = new ArrayDeque<>();
    private AtomicLong idGenerator = new AtomicLong();
    private int capacity;

    public Storage(int capacity) {
        this.capacity = capacity;
    }

    public synchronized void addItem(T item) throws InterruptedException {
        if (this.item.size() == capacity) {
            wait();
        } else if (this.item.size() == 0) {
            notify();
        }
        item.setId(idGenerator.incrementAndGet());
        this.item.push(item);
    }

    public synchronized T getItem() throws InterruptedException {
        if (item.isEmpty()) {
            wait();
        } else if (this.item.size() == capacity) {
            notify();
        }
        return item.pop();
    }

    public int getNumberItems() {
        return item.size();
    }

    public int getCapacity() {
        return capacity;
    }
}
