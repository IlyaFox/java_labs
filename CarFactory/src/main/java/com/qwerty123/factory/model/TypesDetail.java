package com.qwerty123.factory.model;

public enum TypesDetail {
    BODY,
    ENGINE,
    ACCESSORY
}
