package com.qwerty123.factory.model;

public interface Identifiable {
    void setId(long id);
    long getId();
}
