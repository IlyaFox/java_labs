package com.qwerty123.factory.model;

public class Detail implements Identifiable{
    private TypesDetail detail;
    private long id;

    public Detail(TypesDetail detail){
        this.detail = detail;
    }

    public TypesDetail getType(){
        return detail;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public long getId(){
        return id;
    }
}
